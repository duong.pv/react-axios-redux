import * as Types from './../contants/ActionTypes'

const initialState = []

const productReducer = (state=initialState, action) => {
    switch(action.type){
        case Types.FETCH_PRODUCTS:
            state = action.products
            return [...state]

        case Types.DELETE_PRODUCT:
            state = state.filter((prod, index) => {
                return !(prod.id === action.id)
            })
            return [...state]

        case Types.ADD_PRODUCT:
            state.push(action.product)
            return [...state]

        case Types.UPDATE_PRODUCT:
            // TODO
            // Debuging
            
            return [...state]

        default:
            return [...state]
    }
}

export default productReducer