import React, { Component } from 'react';
import './App.css';
import Menu from './components/Menu/Menu' 
import routes from './routes'
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom'

class App extends Component {
    render() {
        return (
            <Router>
                <div>
                    <Menu />
                    <div className="container">
                        <div className="row">
                            {this.showContentMenu(routes)}
                        </div>
                    </div>
                </div>
            </Router>
        );
    }

    showContentMenu = routes => {
        let result = null
        if (routes.length > 0) {
            result = routes.map((item, index) => {
                return <Route path={item.path} component={item.main} exact={item.exact} key={index} />
            })
        }

        return <Switch>{result}</Switch>
    }
}

export default App;
