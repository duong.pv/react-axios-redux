import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import * as actions from './../../actions/index'
import { connect } from 'react-redux'

class ProductActionPage extends Component {
    constructor(props) {
        super(props)
        this.state = {
            id: '',
            name: '',
            price: 0,
            status: false
        }
    }

    componentDidMount() {
        let { match } = this.props
        if (match) {
            let id = match.params.id
            this.props.getEditProduct(id)
        }
    }

    componentWillReceiveProps(nextProps){
        let { itemEditing } = nextProps
        this.setState({
            id: itemEditing.id,
            name: itemEditing.name,
            price: itemEditing.price,
            status: itemEditing.status
        })
    }

    onChange = event => {
        let name = event.target.name
        let value = name === 'status' ? event.target.checked : event.target.value
        this.setState({
            [name]: value
        })
    }

    onSubmit = (event) => {
        event.preventDefault()
        let { id, name, price, status } = this.state
        let { history } = this.props
    
        if (id) {
            this.props.updateProduct({
                id,
                name,
                price,
                status
            })
        }
        else {
            this.props.addProduct({
                name,
                price,
                status
            })
        }
        history.goBack()
    }

    render() {
        let { name, price, status } = this.state
        return (
            <div className="row">
                <div className="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                    <form onSubmit={this.onSubmit}>
                        <div className="form-group">
                            <label>Ten san pham : </label>
                            <input type="text" className="form-control" name="name" value={name} onChange={this.onChange} />
                        </div>

                        <div className="form-group">
                            <label>Gia : </label>
                            <input type="number" className="form-control" name="price" value={price} onChange={this.onChange} />
                        </div>

                        <div className="form-group">
                            <label>Trang thai : </label>
                        </div>

                        <div className="checkbox">
                            <label>
                                <input type="checkbox" name="status" value={status} onChange={this.onChange} checked={status} />
                                Con hang
                            </label>
                        </div>
                        <Link to="/products" className="btn btn-danger mr-10">Quay lại</Link>
                        <button type="submit" className="btn btn-primary">Save</button>
                    </form>

                </div>
            </div>

        );
    }
}

const mapStateToProps = state => {
    return {
        itemEditing: state.itemEditing,
        products: state.products
    }
}

const mapDispatchToProps = (dispatch, props) => {
    return {
        addProduct: product => {
            dispatch(actions.actAddProductRequest(product))
        },
        getEditProduct: id => {
            dispatch(actions.actGetProductRequest(id))
        },
        updateProduct: product => {
            dispatch(actions.actUpdateProductRequest(product))
        }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(ProductActionPage);
