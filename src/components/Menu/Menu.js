import React, { Component } from 'react'
import { Route, Link } from 'react-router-dom'

const menus = [
    {
        name: 'Home',
        to: '/',
        exact: true
    },
    {
        name: 'Products',
        to: '/products',
        exact: false
    }
]

const MenuLink = ({ label, to, activeOnlyWhenExact }) => {
    return (
        <Route path={to} exact={activeOnlyWhenExact} children={({ match }) => {
            let active = match ? 'active' : ''
            return (
                <li className={active}>
                    <Link to={to}>{label}</Link>
                </li> 
            )
        }} />
    )
};

class Menu extends Component {
    render() {
        return (
            <div className="navbar navbar-default">
                <a className="navbar-brand">Call apis</a>
                <ul className="nav navbar-nav">
                    { this.showMenu(menus) }
                </ul>
            </div>
        );
    }

    showMenu = menus => {
        let result = null
        if(menus.length > 0){
            result = menus.map((item, index) => {
                return <MenuLink label={item.name} key={index} to={item.to} activeOnlyWhenExact={item.exact} />
            })
        }        
        return result
    }
}

export default Menu;
